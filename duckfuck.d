// DuckFuck: BrainFuck and FuckFuck Library
// Converts BrainFuck and FuckFuck to D at compile-time.
// 
// Written in the D Programming Language
// Author: Nick Sabalausky

module duckfuck;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.stdio;
import std.string;
import std.traits;
import std.uni;
import std.utf;

enum duckfuckVersionStr   = "0.2";
immutable duckfuckVersion = [0,2];

private template isNull(T)
{
	enum isNull = is(T == typeof(null));
}

class DuckFuckException : Exception
{
	this(string msg)
	{
		super(msg);
	}
}

final class Stack(T)
{
	private T[] data;
	@property size_t capacity()
	{
		return data.length;
	}
	private size_t _length=0;
	@property size_t length()
	{
		return _length;
	}
	
	this(size_t initialCapacity=1024)
	{
		data.length = initialCapacity;
	}
	
	private this(Stack!T s)
	{
		data = s.data.dup;
		_length = s._length;
	}
	
	ref T opIndex(size_t i)
	{
		debug if(i >= _length)
			throw new Exception("Invalid index");
		
		return data[i];
	}
	
	T[] opSlice(size_t a, size_t b)
	{
		debug if(a >= _length || b > _length)
			throw new Exception("Invalid index");
		
		return data[a..b];
	}
	
	private void expand()
	{
		size_t numMore = data.length;
		if(numMore == 0)
			numMore = 1;
		data.length += numMore;
	}
	
	void clear()
	{
		_length = 0;
		data.clear();
	}
	
	void opOpAssign(string op)(T item) if(op=="~")
	{
		if(_length == data.length)
			expand();

		data[_length] = item;
		_length++;
	}
	
	void opOpAssign(string op)(T[] items) if(op=="~")
	{
		while(_length + items.length >= data.length)
			expand();

		data[ _length .. _length + items.length ] = items;
		_length += items.length;
	}
	
	void pop(size_t num=1)
	{
		debug if(num > _length)
			throw new Exception("Invalid index");
			
		_length -= num;
	}
	
	@property ref T top()
	{
		return data[_length-1];
	}
	
	@property bool empty()
	{
		return _length == 0;
	}
	
	void compact()
	{
		data.length = _length;
	}
	
	@property Stack!T dup()
	{
		return new Stack!T(this);
	}
	
	int opApply(int delegate(ref T) dg)
	{
		int result = 0;
		foreach(ref T item; data[0.._length])
		{
			result = dg(item);
			if(result)
				break;
		}
		return result;
	}
	
	int opApply(int delegate(ref size_t, ref T) dg)
	{
		int result = 0;
		foreach(size_t i, ref T item; data[0.._length])
		{
			result = dg(i, item);
			if(result)
				break;
		}
		return result;
	}
}

struct BrainFuckFrontend
{
	void init()
	{
		// Do nothing
	}
	
	// Returns: Done?
	bool step(Backend)(ref Backend backend, ref string source, size_t currIndex, ref size_t nextIndex)
	{
		// The only characters BrainFuck cares about are ASCII, so UTF-8
		// decoding is unnecessary to work correctly on UTF-8 source.

		if(currIndex == source.length)
			return true;

		char c = source[currIndex];
		//nextIndex++;
		backend.nextIndex++; // Workaround for DMD Bugzilla #7266

		switch(c)
		{
		case '>': backend.pointerRight(); break;
		case '<': backend.pointerLeft();  break;
		case '+': backend.increment();    break;
		case '-': backend.decrement();    break;
		case '.': backend.output();       break;
		case ',': backend.input();        break;
		case '[': backend.loopStart();    break;
		case ']': backend.loopEnd();      break;
		default: break;
		}
		
		return false;
	}
}

struct FuckFuckFrontend
{
	// Current parse mode
	private enum Mode
	{
		Normal,
		Comment,

		Arse,
		BoobButt,
		Cock,
		Fuck,
		Knob,
		Shag,
		Tits,
	}
	Mode mode;
	
	// Not counting whitespace
	// Doesn't apply to Mode.Normal or Mode.Comment
	size_t tokenSize;
	
	bool prevCommandExists;
	void delegate() prevCommand;
	
	void init()
	{
		mode = Mode.Normal;
		tokenSize = 0; 
	}

	// This should be a nested func inside step, but it's here
	// to work around DMD Bugzilla #7277
	void setMode(Mode mode)
	{
		this.mode = mode;
		tokenSize = 1;
	}
	
	// This should be a nested func inside step, but it's here
	// to work around DMD Bugzilla #7277
	size_t isNextChar_nextIndex; // Mirrors step's local var nextIndex
	string isNextChar_source; // Mirrors step's local var source
	bool isNextChar(dchar ch)
	{
		return
			isNextChar_nextIndex < isNextChar_source.length &&
			isNextChar_source[isNextChar_nextIndex] == ch;
	}

	// This should be a nested func inside step, but it's here
	// to work around DMD Bugzilla #7277
	void action(void delegate() dg, bool repeatable=true)
	{
		prevCommand = dg;
		prevCommandExists = repeatable;
		dg();
		setMode(Mode.Normal);
	}

	// Returns: Done?
	bool step(Backend)(ref Backend backend, ref string source, size_t currIndex, ref size_t nextIndex)
	{
		if(currIndex >= source.length)
			return true;

		dchar c;
		auto next = backend.nextIndex;
		try
		{
			c = decode(source, next);
			backend.nextIndex = next;
		}
		catch(UTFException e)
		{
			c = cast(uint)cast(ubyte)(source[currIndex]);
			backend.nextIndex++;
		}

		if(isWhite(c))
			return false;

		// Part of DMD Bugzilla #7277 workaround
		isNextChar_nextIndex = backend.nextIndex;
		if(__ctfe)
			isNextChar_source = source.idup; // I don't know why DMD expects this idup
		else
			isNextChar_source = source;
		
		switch(mode)
		{
		case Mode.Normal:
			switch(c)
			{
			case 'A','a': setMode(Mode.Arse);     break;
			case 'B','b': setMode(Mode.BoobButt); break;
			case 'C','c': setMode(Mode.Cock);     break;
			case 'F','f': setMode(Mode.Fuck);     break;
			case 'K','k': setMode(Mode.Knob);     break;
			case 'S','s': setMode(Mode.Shag);     break;
			case 'T','t': setMode(Mode.Tits);     break;

			case '!':
				if(prevCommandExists)
					prevCommand();
				else
					throw new DuckFuckException("Invalid use of '!'");
				break;

			case '/':
				if(isNextChar('*'))
				{
					backend.nextIndex++;
					setMode(Mode.Comment);
				}
				else
					goto default;
				break;

			default:
				if(isValidDchar(c))
					throw new DuckFuckException("Expected start of command, not '"~to!string(c)~"'");
				else
					throw new DuckFuckException("Expected start of command, not invalid UTF-8 code");
				break;
			}
			break;

		case Mode.Comment:
			if(c == '*' && isNextChar('/'))
			{
				backend.nextIndex++;
				setMode(Mode.Normal);
			}
			break;

		default:
			if(tokenSize < 3)
				tokenSize++;
			else
			{
				switch(mode)
				{
				case Mode.BoobButt:
					if(toLower(c) == 'b')
						action(&backend.increment);
					else if(toLower(c) == 't')
						action(&backend.loopEnd, false);
				break;
				
				case Mode.Arse: if(toLower(c) == 'e') action( &backend.loopStart, false ); break;
				case Mode.Cock: if(toLower(c) == 'k') action( &backend.output           ); break;
				case Mode.Fuck: if(toLower(c) == 'k') action( &backend.pointerRight     ); break;
				case Mode.Knob: if(toLower(c) == 'b') action( &backend.input            ); break;
				case Mode.Shag: if(toLower(c) == 'g') action( &backend.pointerLeft      ); break;
				case Mode.Tits: if(toLower(c) == 's') action( &backend.decrement        ); break;
				
				default:
					throw new DuckFuckException("Internal error: Unhandled mode '"~to!string(mode)~"'");
					break;
				}
				
				if(mode != Mode.Normal)
					throw new DuckFuckException("Invalid command found");
			}
			break;

		}

		return false;
	}
}

// A special backend
// Handles semantic tasks
struct Semantic
{
	void delegate() onChangeLoopDepth;
	bool useOnChangeLoopDepth;
	bool failure;
	bool done;
	private int _loopDepth;
	@property void loopDepth(int value)
	{
		_loopDepth = value;
		if(!__ctfe && useOnChangeLoopDepth)
			onChangeLoopDepth();
	}
	@property int loopDepth()
	{
		return _loopDepth;
	}

	void init()
	{
		loopDepth = 0;
		failure = false;
		done = false;
	}

	void finish()       { /+ do nothing +/ }
	void pointerRight() { /+ do nothing +/ }
	void pointerLeft()  { /+ do nothing +/ }
	void increment()    { /+ do nothing +/ }
	void decrement()    { /+ do nothing +/ }
	void output()       { /+ do nothing +/ }
	void input()        { /+ do nothing +/ }

	void loopStart()
    {
		loopDepth = loopDepth + 1;
    }

	void loopEnd()
    {
		if(loopDepth == 0)
		{
			failure = true;
			done = true;
			if(__ctfe)
				return;
			else
				throw new DuckFuckException("Found ']' without matching '['");
		}

		loopDepth = loopDepth - 1;
    }
}

// A special backend
// Skips to the end of current loop
struct SkipLoop
{
	int loopDepth;
	size_t currIndex;
	size_t nextIndex;
	bool failure;
	bool done;

	void init()
	{
		loopDepth = 0;
		currIndex = 0;
		nextIndex = 0;
		failure = false;
		done = false;
	}

	void finish()       { /+ do nothing +/ }
	void pointerRight() { /+ do nothing +/ }
	void pointerLeft()  { /+ do nothing +/ }
	void increment()    { /+ do nothing +/ }
	void decrement()    { /+ do nothing +/ }
	void output()       { /+ do nothing +/ }
	void input()        { /+ do nothing +/ }

	void loopStart()
	{
		loopDepth++;
	}

	void loopEnd()
	{
		if(loopDepth == 0)
			done = true;
		else
			loopDepth--;
	}
}

struct DBackend(Elem, string inputName, string outputName)
{
	string code;
	bool failure;
	size_t currIndex;
	size_t nextIndex;
	Semantic semantic;
	
	bool _done;
	@property bool done()
	{
		return _done || semantic.done;
	}
	@property void done(bool value)
	{
		_done = value;
		if(!_done)
			semantic.done = false;
	}
	
	void onChangeLoopDepth()
	{
		if(__ctfe)
			_indentation = "\t";
		else
			_indentation = std.array.replicate("\t", semantic.loopDepth+1);

		_nlIndentation = "\n"~_indentation;
	}
	
	string _indentation;
	@property string indentation()
	{
		return _indentation;
	}
	
	string _nlIndentation;
	@property string nlIndentation()
	{
		return _nlIndentation;
	}
	
	private static string clean(string str)
	{
		if(__ctfe)
			return str;
		else
		{
			str = str.outdent().strip();
			return str ~ "\n";
		}
	}

	private string indent(string str)
	{
		if(__ctfe)
			return str;
		else
			return indentation ~ str.replace("\n", nlIndentation);
	}

	void init()
	{
		failure = false;
		done = false;
		currIndex = 0;
		nextIndex = 0;
		semantic.onChangeLoopDepth = &onChangeLoopDepth;
		semantic.useOnChangeLoopDepth = true;
		semantic.init();

		code = "{\n";

		code ~= indent(clean(q{
			// Init
			import std.algorithm;
			import std.conv;
			import std.range;
			import std.stdio;
			import std.traits;

			}~Elem.stringof~q{[] _duckfuck_tape;
			size_t _duckfuck_pointer;
			ElementEncodingType!(typeof(_duckfuck_tape)) _duckfuck_cellInit;
		}));

		if(inputName == "")
		{
			code ~= indent(clean(q{
				immutable(ElementEncodingType!(typeof(_duckfuck_tape)))[] _duckfuck_input_stdin;
				string _duckfuck_input_raw;
				bool   _duckfuck_input_eof;
			}));
		}

		code ~= indent(initCode());
	}
	void finish()
	{
		semantic.finish();
		code ~= indent(finishCode());
		code ~= "}\n";
	}
	void pointerRight()
	{
		semantic.pointerRight();
		code ~= indent(pointerRightCode());
	}
	void pointerLeft()
	{
		semantic.pointerLeft();
		code ~= indent(pointerLeftCode());
	}
	void increment()
	{
		semantic.increment();
		code ~= indent(incrementCode());
	}
	void decrement()
	{
		semantic.decrement();
		code ~= indent(decrementCode());
	}
	void output()
	{
		semantic.output();
		code ~= indent(outputCode());
	}
	void input()
	{
		semantic.input();
		code ~= indent(inputCode());
	}
	void loopStart()
	{
		semantic.loopStart();
		code ~= indent(loopStartCode());
	}
	void loopEnd()
	{
		semantic.loopEnd();
		code ~= indent(loopEndCode());
	}

	private static enum currCell = "_duckfuck_tape[_duckfuck_pointer]";
	private static enum eofValue = "_duckfuck_cellInit";

	static string initCode()
	{
		string code;
		
		code ~= clean(q{
			_duckfuck_pointer = 0;

			static if(isSomeChar!(ElementEncodingType!(typeof(_duckfuck_tape))))
				_duckfuck_cellInit = '\x00';
			else
				_duckfuck_cellInit = ElementEncodingType!(typeof(_duckfuck_tape)).init;

			_duckfuck_tape.length = 10*1024;
			// Slow, but works for now:
			foreach(ref ElementEncodingType!(typeof(_duckfuck_tape)) _duckfuck_elem; _duckfuck_tape)
				_duckfuck_elem = _duckfuck_cellInit;
		});

		static if(inputName == "")
		{
			code ~= clean(q{
				_duckfuck_input_stdin = null;
				_duckfuck_input_raw = null;
				_duckfuck_input_eof = false;
			});
		}
		
		return code;
	}
	
	static string finishCode()
	{
		string code;

		static if(outputName == "")
			code ~= clean("stdout.flush();");
		
		return code;
	}
	
	static string pointerRightCode()
    {
        return clean(q{
			// pointerRight
			_duckfuck_pointer++;
			if(_duckfuck_pointer >= _duckfuck_tape.length)
				throw new DuckFuckException("Past end of tape");
		});
    }

	static string pointerLeftCode()
    {
        return clean(q{
			// pointerLeft
			if(_duckfuck_pointer == 0)
				throw new DuckFuckException("Past start of tape");
			_duckfuck_pointer--;
		});
    }

	static string incrementCode()
    {
        return clean("++"~currCell~";");
    }

	static string decrementCode()
    {
        return clean("--"~currCell~";");
    }

	static string outputCode()
    {
		// stdout?
		static if(outputName == "")
		{
			return clean(q{
				// output
				static if(isSomeChar!(ElementEncodingType!(typeof(_duckfuck_tape))))
					write(_duckfuck_tape[_duckfuck_pointer]);
				else
					stdout.rawWrite(_duckfuck_tape[_duckfuck_pointer.._duckfuck_pointer+1]);
			});
		}
		else
		{
			return clean("
				// output
				static if(isArray!(typeof("~outputName~")))
					"~outputName~" ~= _duckfuck_tape[_duckfuck_pointer];
				else
					"~outputName~"(_duckfuck_tape[_duckfuck_pointer]);
			");
		}
    }

	static string inputCode()
    {
		// stdin?
		static if(inputName == "")
		{
			return clean(`
				// input
				if(_duckfuck_input_eof)
					_duckfuck_tape[_duckfuck_pointer] = _duckfuck_cellInit;
				else if(_duckfuck_input_stdin.length == 0)
				{
					_duckfuck_input_raw = readln();
					
					static if(isSomeChar!(ElementEncodingType!(typeof(_duckfuck_tape))))
						_duckfuck_input_stdin = to!(immutable(ElementEncodingType!(typeof(_duckfuck_tape)))[])(_duckfuck_input_raw);
					else
						_duckfuck_input_stdin = *(cast(immutable(ElementEncodingType!(typeof(_duckfuck_tape)))[]*)cast(void*)&_duckfuck_input_raw);
						
					if(_duckfuck_input_stdin.length == 0)
					{
						_duckfuck_input_eof = true;
						_duckfuck_input_stdin = [`~eofValue~`];
					}
				}
				// Convert windows-style (\r\n) newlines to unix-style (\n)
				static if(isSomeChar!(ElementEncodingType!(typeof(_duckfuck_tape))))
				{
					if(_duckfuck_input_stdin[0] == '\r' && _duckfuck_input_stdin.length > 1 && _duckfuck_input_stdin[1] == '\n')
						_duckfuck_input_stdin = _duckfuck_input_stdin[1..$];
				}
				_duckfuck_tape[_duckfuck_pointer] = _duckfuck_input_stdin[0];
				_duckfuck_input_stdin = _duckfuck_input_stdin[1..$];
			`);
		}
		else
		{
			return clean(`
				// input
				static if(isArray!(typeof(`~inputName~`)))
				{
					if(`~inputName~`.length > 0)
					{
						_duckfuck_tape[_duckfuck_pointer] = `~inputName~`[0];
						`~inputName~` = `~inputName~`[1..$];
					}
					else
						_duckfuck_tape[_duckfuck_pointer] = `~eofValue~`;
				}
				else
					_duckfuck_tape[_duckfuck_pointer] = `~inputName~`();
			`);
		}
    }

	static string loopStartCode()
    {
        return clean("
			while("~currCell~" != _duckfuck_cellInit)
			{
		");
    }

	static string loopEndCode()
    {
        return clean("}");
    }
}

struct InterpreterBackend(Frontend, Elem, TInput, TOutput, InElem, OutElem, alias inputFunc, alias outputFunc)
{
	static if(isNull!TInput)
		static enum inputName = "";
	else static if(isArray!TInput)
	{
		TInput* inputRangePtr;
		static enum inputName = "(*inputRangePtr)";
	}
	else static if(isCallable!TInput)
		static enum inputName = "inputFunc";
	else
		static assert(false, "input must be null, array, or callable");

	static if(isNull!TOutput)
		enum outputName = "";
	else static if(isArray!TOutput)
	{
		TOutput* outputRangePtr;
		static enum outputName = "(*outputRangePtr)";
	}
	else static if(isCallable!TOutput)
		static enum outputName = "outputFunc";
	else
		static assert(false, "input must be null, array, or callable");

	alias DBackend!(Elem, inputName, outputName) TDBackend;
	
	bool _done;
	@property bool done()
	{
		return _done || dBackend.done;
	}
	@property void done(bool value)
	{
		_done = value;
		if(!_done)
			dBackend.done = false;
	}

	//string eofValue;
	//string currCell;
	bool failure;
	string source;
	size_t currIndex;
	size_t nextIndex;

	Elem[] _duckfuck_tape;
	size_t _duckfuck_pointer = 0;
	Elem   _duckfuck_cellInit;
	TDBackend dBackend;

	static if(isNull!TInput)
	{
		immutable(Elem)[] _duckfuck_input_stdin;
		string _duckfuck_input_raw;
		bool   _duckfuck_input_eof = false;
	}
	
	// Index into orignal source for the start of each still-open loop
	// (not including the loopStart operator).
	Stack!size_t loopStack;

	this(ref TInput input, ref TOutput output, string source)
	{
		static if(isArray!TInput)
			inputRangePtr = &input;

		static if(isArray!TOutput)
			outputRangePtr = &output;
		
		this.source = source;
		dBackend = TDBackend();
	}
	
	void init()
	{
		failure = false;
		done = false;
		_duckfuck_tape = null;
		_duckfuck_pointer = 0;
		loopStack = new Stack!size_t();

		dBackend.semantic.init();
		mixin(TDBackend.initCode());
	}
	void finish()
	{
		dBackend.semantic.finish();
		mixin(TDBackend.finishCode());
	}
	void pointerRight()
    {
		dBackend.semantic.pointerRight();
		mixin(TDBackend.pointerRightCode());
    }
	void pointerLeft()
    {
		dBackend.semantic.pointerLeft();
		mixin(TDBackend.pointerLeftCode());
    }
	void increment()
    {
		dBackend.semantic.increment();
		mixin(TDBackend.incrementCode());
    }
	void decrement()
    {
		dBackend.semantic.decrement();
		mixin(TDBackend.decrementCode());
    }
	void output()
    {
		dBackend.semantic.output();
		mixin(TDBackend.outputCode());
    }
	void input()
    {
		dBackend.semantic.input();
		mixin(TDBackend.inputCode());
    }
	void loopStart()
    {
		if(_duckfuck_tape[_duckfuck_pointer] == _duckfuck_cellInit)
		{
			SkipLoop skipLoop;
			skipLoop.nextIndex = nextIndex;
			process(Frontend(), skipLoop, source);
			nextIndex = skipLoop.nextIndex;
		}
		else
		{
			loopStack ~= nextIndex;
			dBackend.semantic.loopStart();
		}
    }
	void loopEnd()
    {
		dBackend.semantic.loopEnd();
		if(dBackend.semantic.failure)
		{
			failure = true;
			return;
		}

		if(_duckfuck_tape[_duckfuck_pointer] == _duckfuck_cellInit)
			loopStack.pop();
		else
		{
			nextIndex = loopStack.top;
			dBackend.semantic.loopStart();
		}
    }
}

private alias ubyte DefaultElementType;

private template duckfuckImpl(string frontend, string backend, TInput, TOutput, alias input, alias output, string source)
{
	private template isValidType(T)
	{
		enum isValidType = isArray!T || isCallable!T || isNull!T;
	}
	
	static if(!isValidType!TInput)
	{
		private alias void InElem;
		static assert(false, "input must be an array, callable, or null: Not '"~TInput.stringof~"'");
	}
	
	static if(!isValidType!TOutput)
	{
		private alias void OutElem;
		static assert(false, "output must be an array, callable, or null: Not '"~TOutput.stringof~"'");
	}
	
	static if(isArray!TInput)
		private alias ElementEncodingType!TInput InElem;
	else static if(isCallable!TInput)
	{
		private alias ReturnType!TInput InElem;
		static assert(
			ParameterTypeTuple!(TInput).length == 0 &&
			!is(ReturnType!TInput == void),
			"A callable input must return a value and take no parameters"
		);
	}
	
	static if(isArray!TOutput)
		private alias ElementEncodingType!TOutput OutElem;
	else static if(isCallable!TOutput)
	{
		static assert(
			ParameterTypeTuple!(TOutput).length == 1,
			"A callable output must take exactly one parameter"
		);
		private alias ParameterTypeTuple!(TOutput)[0] OutElem;
	}
	
	static if( !isNull!TInput && !isNull!TOutput )
	{
		static assert(
			is(Unqual!InElem == Unqual!OutElem),
			"\n"~
			"Input and output elements must match:\n"~
			"Input element:  "~(Unqual!InElem ).stringof~"\n"~
			"Output element: "~(Unqual!OutElem).stringof~"\n"
		);
	}
	
	static if( !isNull!TInput )
		private alias Unqual!InElem TapeElem;
	else static if( !isNull!TOutput )
		private alias Unqual!OutElem TapeElem;
	else
		private alias DefaultElementType TapeElem;

	static if(isNull!TInput)
		private alias TapeElem InElem;
	static if(isNull!TOutput)
		private alias TapeElem OutElem;

	//TODO: Make this more user-extended-friendly
	static if(backend == "d")
	{
		static if( isNull!TInput )
			private enum inputParam = `""`;
		else
			private enum inputParam = "__traits(identifier, input)";
		
		static if( isNull!TOutput )
			private enum outputParam = `""`;
		else
			private enum outputParam = "__traits(identifier, output)";

		private enum impl = frontend~"ToD!(TapeElem, "~inputParam~", "~outputParam~")(source)";
		immutable result = mixin(impl);
	}
	else
	{
		static if( isNull!TInput )
			private enum inputParam = "dummy";
		else
			private enum inputParam = "input";

		static if( isNull!TOutput )
			private enum outputParam = "dummy";
		else
			private enum outputParam = "output";

		private enum result =
			"typeof(null) dummy;\n"~
			frontend~"Interpreter!("~TapeElem.stringof~", typeof(input), typeof(output), "~InElem.stringof~", "~OutElem.stringof~", input, output)("~inputParam~", "~outputParam~", "~source~");";
	}
}

string brainfuck(string source)()
{
	// Empty strings instead of null to work around DMD Bugzilla #7278
	return brainfuckToD!(DefaultElementType, "", "")(source);
}
string fuckfuck(string source)()
{
	// Empty strings instead of null to work around DMD Bugzilla #7278
	return fuckfuckToD!(DefaultElementType, "", "")(source);
}

string brainfuck(alias input, alias output, string source)()
{
	return
		duckfuckImpl!("brainfuck", "d", typeof(input), typeof(output), input, output, source)
		.result;
}
string fuckfuck(alias input, alias output, string source)()
{
	return
		duckfuckImpl!("fuckfuck", "d", typeof(input), typeof(output), input, output, source)
		.result;
}

void brainfuck()(string source)
{
	brainfuck!(null, null)(source);
}
void fuckfuck()(string source)
{
	fuckfuck!(null, null)(source);
}
void brainfuck(alias input, alias output)(string source)
{
	mixin( duckfuckImpl!("brainfuck", "interpreter", typeof(input), typeof(output), input, output, "source").result );
}
void fuckfuck(alias input, alias output)(string source)
{
	mixin( duckfuckImpl!("fuckfuck", "interpreter", typeof(input), typeof(output), input, output, "source").result );
}

string brainfuckToD(Elem, string inputName, string outputName)(string source)
{
	auto backend = DBackend!(Elem, inputName, outputName)();
	process(BrainFuckFrontend(), backend, source);
	return backend.code;
}

string fuckfuckToD(Elem, string inputName, string outputName)(string source)
{
	auto backend = DBackend!(Elem, inputName, outputName)();
	process(FuckFuckFrontend(), backend, source);
	return backend.code;
}

//TODO: Better encapsulate IO type checking/analysis for reuse (like by custom backends)
void brainfuckInterpreter
	(Elem, TInput, TOutput, InElem, OutElem, alias inputFunc, alias outputFunc)
	(ref TInput input, ref TOutput output, string source)
{
	auto backend = InterpreterBackend!(BrainFuckFrontend, Elem, TInput, TOutput, InElem, OutElem, inputFunc, outputFunc)(input, output, source);
	process(BrainFuckFrontend(), backend, source);
}

void fuckfuckInterpreter
	(Elem, TInput, TOutput, InElem, OutElem, alias inputFunc, alias outputFunc)
	(ref TInput input, ref TOutput output, string source)
{
	auto backend = InterpreterBackend!(FuckFuckFrontend, Elem, TInput, TOutput, InElem, OutElem, inputFunc, outputFunc)(input, output, source);
	process(FuckFuckFrontend(), backend, source);
}

void process(Frontend, Backend)(Frontend frontend, ref Backend backend, string source)
{
	backend.init();
	while(
		!frontend.step(backend, source, backend.currIndex, backend.nextIndex) &&
		!backend.done
	)
	{
		backend.currIndex = backend.nextIndex;
	}
	backend.finish();
}
