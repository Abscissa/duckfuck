// Run DuckFuck Tests
// 
// Written in the D Programming Language
// Author: Nick Sabalausky

import std.array;
import std.conv;
import std.file;
import std.path;
import std.process;
import std.stdio;
import std.string;
import duckfuck;

enum dirSep = dirSeparator;

version(Win32)
	import std.c.windows.windows;
else version(OSX)
	private extern(C) int _NSGetExecutablePath(char* buf, uint* bufsize);
else version(linux)
{
	import std.c.linux.linux;
	enum selfExeLink = "/proc/self/exe";
}
else
{
	import std.c.linux.linux;
	enum selfExeLink = "/proc/curproc/file";
}

string getExec()
{
	auto file = new char[4*1024];
	size_t filenameLength;
	version(Win32)
		filenameLength = GetModuleFileNameA(null, file.ptr, file.length-1);
	else version(OSX)
	{
		filenameLength = file.length-1;
		_NSGetExecutablePath(file.ptr, &filenameLength);
	}
	else
        filenameLength = readlink(toStringz(selfExeLink), file.ptr, file.length-1);

	return to!string(file[0..filenameLength]);
}

string getExecPath()
{
	return getExec().dirname() ~ "/";
}

int numFailures;
string execPath;

//TODO: Test with more types than just char.
//TODO: Run only specially-chosen tests by default.

int main(string[] args)
{
	if(args.length != 1 && args.length != 6)
	{
		writeln("Usage: runtests [language testname backend inputmode outputmode]");
		writeln("Examples:");
		writeln("  runtests   # Run all tests");
		writeln("  runtests brainfuck hello toD std array      # Run one test");
		writeln("  runtests fuckfuck echo interpret func std   # Run one test");
		writeln("  runtests fuckfuck echo interpret omit omit  # Run one test");
		return 1;
	}
	
	numFailures = 0;
	execPath = getExecPath();
	
	if(args.length == 6)
	{
		runSingle(args[1], args[2], args[3], args[4], args[5]);
	}
	else
	{
		enum ioModes = ["std", "array", "func"];

		foreach(lang; ["brainfuck", "fuckfuck"])
		foreach(string test; dirEntries(execPath~"testdata/"~lang, SpanMode.shallow))
		if(isDir(test))
		foreach(backend; ["toD", "interpret"])
		{
			test = baseName(test);
			writeln("Testing: ", lang, "/", test, " (", backend, ")");

			runSingle(lang, test, backend, "omit", "omit");
			
			foreach(input; ioModes)
			foreach(output; ioModes)
				runSingle(lang, test, backend, input, output);
		}
	}
	
	if(numFailures)
	{
		writeln("# Failures: ", numFailures);
		return 1;
	}
	else
	{
		writeln("All tests passed!");
		return 0;
	}
}

void runSingle(string lang, string test, string backend, string input, string output)
{
	write("IO=", input, "/", output, "...");
	stdout.flush();

	// Validate Params
	if((input=="omit" || output=="omit") && input != output)
	{
		writeln("Mode 'omit' can only be used for BOTH input and output, not just one.");
		numFailures++;
	}
	
	//TODO: On Win, fix to work with different drive letter
	//TODO: Fix this (currently does nothing):
	system("cd "~execPath);
	
	//TODO: Don't use file redirection once std.process finally gets its rewrite.
	// The '-release' flag is a workaround for DMD Issue #854
	// Compile
	int errLevel;
	auto cmd = 
		"testdata"~dirSep~"redirect-compile dmd testdata/runSingleTest.d duckfuck.d"~
		" -version=Lang_"~lang~" -version=Backend_"~backend~
		" -version=InputMode_"~input~" -version=OutputMode_"~output~
		" -version=Type_char -release"~
		" -Jtestdata/"~lang~"/"~test;
	errLevel = system(cmd);

	// Compiled?
	if(errLevel != 0)
	{
		numFailures++;
		writeln("Failed to Compile:");
		writeln(cmd);
		writeln(cast(string)read("testdata/out.dat"));
		return;
	}
	
	// Setup stdin
	if(output == "omit" || output == "std")
	{
		auto inputData = read("testdata/"~lang~"/"~test~"/input");
		std.file.write("testdata/in.dat", inputData);
	}
	
	// Run test
	version(Windows)
		enum singleTestExec = "runSingleTest";
	else
		enum singleTestExec = "./runSingleTest";
	errLevel = system("testdata"~dirSep~"redirect-test "~singleTestExec);
	
	// Returned Failure?
	if(errLevel != 0)
	{
		numFailures++;
		writeln("Wrong Result!");
		return;
	}

	// Check stdout
	if(output == "omit" || output == "std")
	{
		auto outputData   = cast(string)read("testdata/out.dat");
		auto expectedData = cast(string)read("testdata/"~lang~"/"~test~"/expected");
		
		//TODO: Only do this if type is some char type
		version(Windows)
			outputData = outputData.replace("\r\n", "\n");
		
		if(outputData != expectedData)
		{
			numFailures++;
			writeln("Wrong Result!");
			return;
		}
	}
	
	writeln("ok");
}
