// DuckFuck Sample
// 
// Written in the D Programming Language
// Author: Nick Sabalausky

import std.stdio;
import std.string;
import duckfuck;

void main()
{
	auto echoThis = "Echo this!\n";

	// Hello World! Converted from brainfuck to D at compile-time.
	mixin(brainfuck!"
		>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]
		<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.
	");
	writeln();

	// Hello World! Interpreted brainfuck at runtime.
	brainfuck("
		  This is a basic hello world in brainfuck

		>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]
		<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.

		   Neat huh?
	");
	writeln();

	// Echo. Converted from fuckfuck to D at compile-time.
	mixin(fuckfuck!(echoThis, null, "
	/* This is 'echo' in fuckfuck */

		knob
ARSE
	/* Inside the loop */
	COCKKNOB
B U T T

	"));

	// Hello World! Interpreted fuckfuck at runtime.
	fuckfuck("
		/* Ain't fuckfuck fun? :) */

		fuck BOOB!!!!!!!!
		ARSEshagBOOB!!!!!!!
		fuck TITS BUTT
		shag cock fuck BOOB!!!!!!
		ARSEshagBOOB!!!
		fuck TITS BUTT
		shag BOOB cock BOOB!!!!!!
		cock! BOOB!! cock fuck!!
		BOOB!!!!!!!
		
		/* a little bit of fucking censoring... */
		ANNE s * *g BARB!!!

		fuck TITS BUTT

		shag cock FUCK!!
		BOOB!!!!!!!!!
		ARSE shag
		BOOB!!!!!!!!
		fuck TITS BUTT
		shag TITS!!
		cock
		shag shag shag shag
		cock
		BOOB! BOOB cock
		TITS!!!!! cock TITS!!!!!!!
		cock FUCK! BOOBcock
	");
	writeln();

	// Echo. Using functions for I/O. Conveted from brainfuck to D at compile-time.
	int inputIndex = 0;
	char generator()
	{
		string str = "testing\nnext line";
		char c;
		if(inputIndex >= str.length)
			c = '\x00';
		else
		{
			c = str[inputIndex];
			inputIndex++;
		}
		return c;
	}
	void sink(char c)
	{
		write(c);
	}
	// Echo is only five-bytes in brainfuck!
	mixin(brainfuck!(generator, sink, ",[.,]"));
}
