DuckFuck v0.2: D-based FuckFuck and More!

A flexible, modular D-based interpreter/conversion system for FuckFuck
and other BrainFuck-based langauges.

Includes the DuckFucker! A command-line tool to interpret any
supported langauge.

----------------------------------------------------

Currently Supported:

Source Languages:

	brainfuck: http://esoteric.voxelperfect.net/wiki/Brainfuck

	fuckfuck:
		http://esoteric.voxelperfect.net/wiki/Fuckfuck
		http://web.archive.org/web/20050318095341/http://www.chilliwilli.co.uk/ff/
		
Backends:
	Compile-time convertion to D
	Runtime Interpreter

----------------------------------------------------

Language Details:

Tape Length:
	10k (ie, 10*1024) elements

Ends of Tape:
	The tape does not wrap around. Hitting the end results in an error.

Cell Initalization:
	The '.init' property of the cell, except for character types
	which are initialized to '\x00' (ie, zero).

Loop Value:
	Loops run while the value under the pointer is '.init', except for
	character types which use '\x00' (ie, zero).

End Of File:
	The value for End Of File is the same as the cell initialization value.

All the above will be configurable in a later version.

Cells:
	All behavior depends on the Cell type.
	The CellType is determined by the input and/or output.
	Default is 8-bit 'ubyte'. See 'CellType' below.
	For DuckFucker, the cell is always D's ubyte type.
	Alternate cell types might be added to DuckFucker later.

----------------------------------------------------

To compile DuckFucker, sample and test suite:

1. Download and install DMD 2.057 or newer.
Try using DVM ( https://bitbucket.org/doob/dvm ) to install DMD:
	> dvm install 2.057
	> dvm use 2.057 --default 

2. Compile:
	> rdmd --build-only duckfucker.d
	> rdmd --build-only sample.d
	> rdmd --build-only runtests.d

3. Use! Here is DuckFucker's help screen:

----------------------------------------------------

Usage: duckfucker [options] sourcefile [options]

Examples:
    duckfucker hello.bf    # BrainFuck
    duckfucker hello.ff    # FuckFuck

    # Take input from 'input.txt' and send output to 'output.txt'
    duckfucker echo.ff < input.txt > output.txt

Options:
    -h, --help         This help screen
    -l, --lang=<lang>  Use langauge <lang>.

Langauge choices are:
    default    (use file extension)
    brainfuck  (or 'bf')
    fuckfuck   (or 'ff')

----------------------------------------------------

To use as a library in D code:

	import duckfuck;

Compile-time conversion to D:

	mixin( brainfuck!"code here" );
	mixin( fuckfuck !"code here" );

Runtime interpretation:

	brainfuck("code here");
	fuckfuck ("code here");

Custom input/output:

	mixin( brainfuck!(input, output, "code here") );
	mixin( fuckfuck !(input, output, "code here") );

	brainfuck!(input, output)("code here");
	fuckfuck !(input, output)("code here");

'input' can be:
	null (ie, use stdin)
	array of CellType
	CellType func()

'output' can be:
	null (ie, use stdout)
	array of CellType
	U func(CellType) (return value ignored, usually 'void')

Ranges might be supported later.

Note: At the moment, using functions for input/output only works for
compile-time conversion to D, not the runtime interpreter.
This will be fixed later.

CellType can be mutable, const or immutable.
CellType *should* support any type that supports all of the following:
	++
	--
	T.init

When using stdin/stdout, your choice of cell types may be limited by std.stdio.

If the CellType is a character type, all Windows-style line endings
(ie, "\r\n") found in the input will be automatically converted to
the Unix-style "\n".

WARNING: If you use an array for input or output in runtime interpreted mode,
you MUST compile with -release to work around DMD Issue #846:
    http://d.puremagic.com/issues/show_bug.cgi?id=846

----------------------------------------------------

Adding new source langauges and backends (better documentation
for this is coming):

Source Langauges:
Follow the leads of:
	struct BrainFuckFrontend
	struct FuckFuckFrontend

Backends:
Just note how the frontends work, and write something
they will accept. Or follow the leads of these:
	struct DBackend
	struct InterpreterBackend

You may also want to pay attention, or even utilize, these "special"
backends which are used internally by the other backends:
	struct Semantic
	struct SkipLoop

Once you've created a new source language or backend, you'll want
to create some helper functions like 'brainfuck' and 'fuckfuck' above.
These helper functions should instantiate the appropriate frontend and
backend and pass them to the 'process' function which will actually run
the user's source code through them.

----------------------------------------------------

Additional notes:

Some of the fuckfuck examples at the following URL don't work yet.
I'm not quite sure why. Doesn't appear to be an issue of tape length,
cell size, or EOF character.
	http://web.archive.org/web/20050310184520/http://www.chilliwilli.co.uk/ff/download.html
