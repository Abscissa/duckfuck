// DuckFucker: BrainFuck and FuckFuck Interpreter in D
// 
// Written in the D Programming Language
// Author: Nick Sabalausky

import std.array;
import std.file;
import getopt = std.getopt;
import std.path;
import std.stdio;
import std.string;

import duckfuck;

void showHelp()
{
	writeln((`
		DuckFucker v`~duckfuckVersionStr~` - BrainFuck and FuckFuck Interpreter in D
		Copyright (c) 2012 Nick Sabalausky
		Licensed under The zlib/libpng License

		Usage: duckfucker [options] sourcefile [options]

		Examples:
			duckfucker hello.bf    # BrainFuck
			duckfucker hello.ff    # FuckFuck
			
			# Take input from 'input.txt' and send output to 'output.txt'
			duckfucker echo.ff < input.txt > output.txt
		
		Options:
			-h, --help         This help screen
			-l, --lang=<lang>  Use langauge <lang>.

		Langauge choices are:
			default    (use file extension)
			brainfuck  (or 'bf')
			fuckfuck   (or 'ff')
	`).outdent().replace("\t", "    ").strip());
}

int main(string[] args)
{
	bool help;
	string lang;
	
	getopt.endOfOptions = "";
	getopt.getopt(args,
		"h|help", &help,
		"l|lang", &lang,
	);
	
	args = args[1..$];
	if(help || args.length != 1)
	{
		showHelp();
		return 0;
	}
	
	try
	{
		auto srcfile = args[0];
		
		if(lang == "" || lang == "default")
		{
			lang = srcfile.extension();
			if(lang.length > 0)
				lang = lang[1..$];
		}

		switch(lang)
		{
		case "bf", "brainfuck":
			brainfuck( cast(string) read(srcfile) );
			break;
		
		case "ff", "fuckfuck":
			fuckfuck( cast(string) read(srcfile) );
			break;

		default:
			throw new DuckFuckException("Unknown language '"~lang~"'");
		}
	}
	catch(DuckFuckException e)
	{
		stderr.writeln(e.msg);
		return 1;
	}
	
	return 0;
}
