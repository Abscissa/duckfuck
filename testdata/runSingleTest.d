// Run Single DuckFuck Test
// 
// Written in the D Programming Language
// Author: Nick Sabalausky
//
// This is a helper tool that is compiled and run by runtests.d
//
// Compile:
// dmd testdata/runSingleTest.d duckfuck.d -version=Backend_toD ... -Jtestdata/{lang}/{testname}

import std.conv;
import std.stdio;
import std.traits;

import duckfuck;

enum Backend
{
	toD, interpret
}

enum Lang
{
	brainfuck, fuckfuck
}

enum IOMode
{
	omit, std, array, func
}

version( Backend_toD       ) enum backend = Backend.toD;
version( Backend_interpret ) enum backend = Backend.interpret;

version( Lang_brainfuck ) enum lang = Lang.brainfuck;
version( Lang_fuckfuck  ) enum lang = Lang.fuckfuck;

version( InputMode_omit  ) version = IOMode_omit;
version( InputMode_std   ) enum inputMode = IOMode.std;
version( InputMode_array ) enum inputMode = IOMode.array;
version( InputMode_func  ) enum inputMode = IOMode.func;

version( OutputMode_omit  ) version = IOMode_omit;
version( OutputMode_std   ) enum outputMode = IOMode.std;
version( OutputMode_array ) enum outputMode = IOMode.array;
version( OutputMode_func  ) enum outputMode = IOMode.func;

version( IOMode_omit )
{
	enum inputMode  = IOMode.omit;
	enum outputMode = IOMode.omit;
}

version( Type_char  ) alias char  Type;
version( Type_wchar ) alias wchar Type;
version( Type_dchar ) alias dchar Type;
version( Type_ubyte ) alias ubyte Type;

enum source         = import("source");
enum inputDataRaw   = import("input");
enum expectedOutput = import("expected");

enum langStr = to!string(lang);

immutable inputData = cast(immutable Type[])inputDataRaw;

static if(isSomeChar!Type)
	enum eof = '\x00';
else
	enum eof = Type.init;

int main()
{
	// Input
	static if(inputMode == IOMode.std)
		enum input = "null";
	else static if(inputMode == IOMode.array)
	{
		Type[] inputArray = inputData.dup;
		enum input = "inputArray";
	}
	else static if(inputMode == IOMode.func)
	{
		enum input = "inputFunc";
		size_t inputIndex = 0;
		Type inputFunc()
		{
			if(inputIndex == inputData.length)
				return eof;

			auto elem = inputData[inputIndex];
			inputIndex++;
			return elem;
		}
	}
	
	// Output
	static if(outputMode == IOMode.std)
		enum output = "null";
	else static if(outputMode == IOMode.array)
	{
		enum output = "outputData";
		Type[] outputData;
	}
	else static if(outputMode == IOMode.func)
	{
		enum output = "outputFunc";
		Type[] outputData;
		void outputFunc(Type elem)
		{
			outputData ~= elem;
		}
	}
	
	// Run Test
	static if(backend == Backend.toD)
	{
		version(IOMode_omit)
			mixin("mixin("~langStr~"!(source));");
		else
			mixin("mixin("~langStr~"!("~input~", "~output~", source));");
	}
	else static if(backend == Backend.interpret)
	{
		version(IOMode_omit)
			mixin(langStr~"(source);");
		else
			mixin(langStr~"!("~input~", "~output~")(source);");
	}
	else
		static assert(false);
	
	// Check Result
	static if(outputMode == IOMode.omit || outputMode == IOMode.std)
	{
		return 0; // runTests will check the result
	}
	else
	{
		if(outputData == expectedOutput)
			return 0;
		else
			return 1;
	}
}
